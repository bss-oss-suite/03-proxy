# 02-proxy
Part of project BSS/OSS suite.

Run the Envoy proxy. The `envoy.yaml` file configures Envoy to listen to browser requests at port `:8080`, and forward them to port `:50051`.

```shell
docker run -d -v "$(pwd)"/envoy.yaml:/etc/envoy/envoy.yaml:ro -p 8080:8080 -p 9901:9901 envoyproxy/envoy:v1.25-latest

```
